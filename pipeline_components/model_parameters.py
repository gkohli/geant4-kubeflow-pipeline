"""
Handling the digestion of Modelling Parameters into the Kubeflow Pipeline
"""
from typing import NamedTuple


def model_input_parameters(original_dim: int, checkpoint_dir: str, batch_size: int, intermediate_dim1: int,
                           intermediate_dim2: int, intermediate_dim3: int, intermediate_dim4: int, latent_dim: int,
                           epsilon_std: int, mu: int, epochs: int, lr: float, outActiv: str, validation_split: float,
                           wkl: float, ki: str, bi: str, earlyStop: bool) -> NamedTuple('Variable_Details',
                                                                                        [('batch_size', int),
                                                                                         ('original_dim', int),
                                                                                         ('intermediate_dim1', int),
                                                                                         ('intermediate_dim2', int),
                                                                                         ('intermediate_dim3', int),
                                                                                         ('intermediate_dim4', int),
                                                                                         ('latent_dim', int),
                                                                                         ('epsilon_std', int),
                                                                                         ('mu', int), ('epochs', int),
                                                                                         ('lr', float),
                                                                                         ('outActiv', str),
                                                                                         ('validation_split', float),
                                                                                         ('wReco', int), ('wkl', float),
                                                                                         ('ki', str),
                                                                                         ('bi', str),
                                                                                         ('earlyStop', bool),
                                                                                         ('checkpoint_dir', str)]):
    batch_size = batch_size
    original_dim = original_dim
    intermediate_dim1 = intermediate_dim1
    intermediate_dim2 = intermediate_dim2
    intermediate_dim3 = intermediate_dim3
    intermediate_dim4 = intermediate_dim4
    latent_dim = latent_dim
    epsilon_std = epsilon_std
    mu = mu
    epochs = epochs
    lr = lr
    outActiv = outActiv
    validation_split = validation_split
    wReco = original_dim
    wkl = wkl
    ki = ki
    bi = bi
    earlyStop = earlyStop
    checkpoint_dir = checkpoint_dir

    return (batch_size, original_dim, intermediate_dim1, intermediate_dim2, intermediate_dim3, intermediate_dim4,
            latent_dim, epsilon_std, mu, epochs, lr, outActiv, validation_split, wReco, wkl, ki, bi, earlyStop,
            checkpoint_dir)