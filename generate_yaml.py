import kfp
from pipeline_components.model_parameters import model_input_parameters
from pipeline_components.generate import generate
from pipeline_components.input_parameters import input_parameters
from pipeline_components.katib_setup import model_setup
from pipeline_components.preprocess import preprocess_new
from pipeline_components.validate import validate
import os


def generate_yaml():
    """
    Generating YAML for each component of the Pipeline
    """
    kfp.components.func_to_container_op(input_parameters, base_image='python:3.7',
                                        output_component_file=f'{os.getcwd()}/components_yaml'
                                                              '/input.yaml')
    kfp.components.func_to_container_op(model_input_parameters,
                                        base_image='gitlab-registry.cern.ch/ai-ml/kubeflow_images'
                                                   '/tensorflow-notebook-gpu-2.1.0:v0.6.1-33',
                                        output_component_file=f'{os.getcwd()}/components_yaml/model_para.yaml')
    kfp.components.func_to_container_op(model_setup, base_image='registry.cern.ch/ml/kf-14-tensorflow-jupyter:v4',
                                        output_component_file=f'{os.getcwd()}/components_yaml/model_setup.yaml')
    kfp.components.func_to_container_op(preprocess_new, base_image='gitlab-registry.cern.ch/gkohli/mlfastsim-kubeflow'
                                                                   '-pipeline/kube_gkohli:cern_pipelinev2',
                                        output_component_file=f'{os.getcwd()}/components_yaml/preprocess.yaml')
    kfp.components.func_to_container_op(generate,
                                        base_image='gitlab-registry.cern.ch/gkohli/mlfastsim-kubeflow-pipeline'
                                                   '/kube_gkohli:cern_pipelinev2',
                                        output_component_file=f'{os.getcwd()}/components_yaml/generate.yaml')
    kfp.components.func_to_container_op(validate,
                                        base_image='gitlab-registry.cern.ch/gkohli/mlfastsim-kubeflow-pipeline'
                                                   '/kube_gkohli:cern_pipelinev2',
                                        output_component_file=f'{os.getcwd()}/components_yaml/validate.yaml')


if __name__ == '__main__':
    try:
        os.mkdir(f'{os.getcwd()}/components_yaml')
    except OSError as error:
        pass
    generate_yaml()
