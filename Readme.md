# Steps to set up the Geant4-FastSim - ML pipeline
>The following steps would provide you a guided workflow through which you can import this project
> onto your Kubeflow Namespace and run the experiments.

`STEP1`: Go to ml.cern.ch and login into the Kubeflow Dashboard

`STEP2`: Go to Notebook tab on the side panel and create a working space

`STEP3`: Confirm the allocated resources and create the workspace with kf-14-tensorflow-jupyter:v1

`STEP4`: Create a folder from the sidebars

`STEP5`: Once inside the folder open a Terminal and <notebook.ipynb> 

> Before Step 6 Create your kerberos secret to access the EOS memory space from inside the Pipeline
> 
> The commands are to be entered in the Terminal as follows:
> 1) `kinit <your namespace>`
> 2) `kubectl delete secret krb-secret`
> 3) `kubectl create secret generic krb-secret --from-file=/tmp/krb5cc_1000`
> 
`STEP6`: `!git clone <repo name>`

`STEP7`: Change the parameter values in the `configuration.py` so to adjust according to your experiment setup

`STEP8`: `!python3 generate_yaml.py`
> The above step would create
YAML files for each python component which will be a part of the Kubeflow Pipeline

`STEP9`: `!python3 main.py --namespace <Specify your namespace name> --pipeline_name <Specify your pipeline name>`

`STEP10`: To check the results open the `runs` tab to see final pipeline graphs and `AutoML` tab to access the Katib Hyper Parameter Tuning 

>A detailed documentation of the complete project would soon be available at the official links!!!



